<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210118011903 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE animal_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE certification_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE commentaire_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE transaction_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE "user_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE animal (id INT NOT NULL, proprio_id INT NOT NULL, espece VARCHAR(255) DEFAULT NULL, race VARCHAR(255) DEFAULT NULL, genre VARCHAR(255) DEFAULT NULL, date_birth DATE DEFAULT NULL, birthplace VARCHAR(255) DEFAULT NULL, prisma_id VARCHAR(255) DEFAULT NULL, create_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, update_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, prix VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_6AAB231F79F37AE5 ON animal (proprio_id)');
        $this->addSql('CREATE TABLE certification (id INT NOT NULL, id_user_id INT NOT NULL, nom VARCHAR(255) NOT NULL, prisma_id VARCHAR(255) DEFAULT NULL, create_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, update_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, path VARCHAR(255) DEFAULT NULL, validation BOOLEAN DEFAULT NULL, date_obtention TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_6C3C6D7579F37AE5 ON certification (id_user_id)');
        $this->addSql('CREATE TABLE commentaire (id INT NOT NULL, id_author_id INT NOT NULL, user_subject_id INT DEFAULT NULL, note VARCHAR(255) DEFAULT NULL, comment VARCHAR(255) DEFAULT NULL, prisma_id VARCHAR(255) DEFAULT NULL, create_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, update_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_67F068BC76404F3C ON commentaire (id_author_id)');
        $this->addSql('CREATE INDEX IDX_67F068BCA5591A6B ON commentaire (user_subject_id)');
        $this->addSql('CREATE TABLE transaction (id INT NOT NULL, acheteur_id INT NOT NULL, vendeur_id INT NOT NULL, type VARCHAR(255) NOT NULL,statut INT DEFAULT NULL, prix VARCHAR(255) NOT NULL, prisma_id VARCHAR(255) DEFAULT NULL, create_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, update_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_723705D18EB576A8 ON transaction (acheteur_id)');
        $this->addSql('CREATE INDEX IDX_723705D120068689 ON transaction (vendeur_id)');
        $this->addSql('CREATE TABLE transaction_animal (transaction_id INT NOT NULL, animal_id INT NOT NULL, PRIMARY KEY(transaction_id, animal_id))');
        $this->addSql('CREATE INDEX IDX_1CD6515F2FC0CB0F ON transaction_animal (transaction_id)');
        $this->addSql('CREATE INDEX IDX_1CD6515F8E962C16 ON transaction_animal (animal_id)');
        $this->addSql('CREATE TABLE "user" (id INT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, name VARCHAR(255) DEFAULT NULL, name_zoo VARCHAR(255) DEFAULT NULL, siret_siren VARCHAR(20) DEFAULT NULL, adresse VARCHAR(255) DEFAULT NULL, cp VARCHAR(125) DEFAULT NULL, ville VARCHAR(125) DEFAULT NULL, pays VARCHAR(255) DEFAULT NULL, telephone VARCHAR(255) DEFAULT NULL, prisma_id VARCHAR(255) DEFAULT NULL, create_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, update_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id), )');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649E7927C74 ON "user" (email)');
        $this->addSql('ALTER TABLE animal ADD CONSTRAINT FK_6AAB231F79F37AE5 FOREIGN KEY (proprio_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE certification ADD CONSTRAINT FK_6C3C6D7579F37AE5 FOREIGN KEY (id_user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE commentaire ADD CONSTRAINT FK_67F068BC76404F3C FOREIGN KEY (id_author_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE commentaire ADD CONSTRAINT FK_67F068BCA5591A6B FOREIGN KEY (user_subject_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE transaction ADD CONSTRAINT FK_723705D18EB576A8 FOREIGN KEY (acheteur_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE transaction ADD CONSTRAINT FK_723705D120068689 FOREIGN KEY (vendeur_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE transaction_animal ADD CONSTRAINT FK_1CD6515F2FC0CB0F FOREIGN KEY (transaction_id) REFERENCES transaction (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE transaction_animal ADD CONSTRAINT FK_1CD6515F8E962C16 FOREIGN KEY (animal_id) REFERENCES animal (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE transaction_animal DROP CONSTRAINT FK_1CD6515F8E962C16');
        $this->addSql('ALTER TABLE transaction_animal DROP CONSTRAINT FK_1CD6515F2FC0CB0F');
        $this->addSql('ALTER TABLE animal DROP CONSTRAINT FK_6AAB231F79F37AE5');
        $this->addSql('ALTER TABLE certification DROP CONSTRAINT FK_6C3C6D7579F37AE5');
        $this->addSql('ALTER TABLE commentaire DROP CONSTRAINT FK_67F068BC76404F3C');
        $this->addSql('ALTER TABLE commentaire DROP CONSTRAINT FK_67F068BCA5591A6B');
        $this->addSql('ALTER TABLE transaction DROP CONSTRAINT FK_723705D18EB576A8');
        $this->addSql('ALTER TABLE transaction DROP CONSTRAINT FK_723705D120068689');
        $this->addSql('DROP SEQUENCE animal_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE certification_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE commentaire_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE transaction_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE "user_id_seq" CASCADE');
        $this->addSql('DROP TABLE animal');
        $this->addSql('DROP TABLE certification');
        $this->addSql('DROP TABLE commentaire');
        $this->addSql('DROP TABLE transaction');
        $this->addSql('DROP TABLE transaction_animal');
        $this->addSql('DROP TABLE "user"');
    }
}
