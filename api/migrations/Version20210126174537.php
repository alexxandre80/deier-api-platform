<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210126174537 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP TABLE transaction_animal');
        $this->addSql('ALTER TABLE transaction ADD animals_id INT NOT NULL');
        $this->addSql('ALTER TABLE transaction ALTER vendeur_id SET NOT NULL');
        $this->addSql('ALTER TABLE transaction ADD CONSTRAINT FK_723705D18E962C16 FOREIGN KEY (animal_id) REFERENCES animal (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_723705D18E962C16 ON transaction (animal_id)');
        $this->addSql('ALTER INDEX idx_723705d18eb576a8 RENAME TO IDX_723705D196A7BB5F');
        $this->addSql('ALTER INDEX idx_723705d120068689 RENAME TO IDX_723705D1858C065E');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('CREATE TABLE transaction_animal (transaction_id INT NOT NULL, animal_id INT NOT NULL, PRIMARY KEY(transaction_id, animal_id))');
        $this->addSql('CREATE INDEX idx_1cd6515f8e962c16 ON transaction_animal (animal_id)');
        $this->addSql('CREATE INDEX idx_1cd6515f2fc0cb0f ON transaction_animal (transaction_id)');
        $this->addSql('ALTER TABLE transaction_animal ADD CONSTRAINT fk_1cd6515f2fc0cb0f FOREIGN KEY (transaction_id) REFERENCES transaction (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE transaction_animal ADD CONSTRAINT fk_1cd6515f8e962c16 FOREIGN KEY (animal_id) REFERENCES animal (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE transaction DROP CONSTRAINT FK_723705D18E962C16');
        $this->addSql('DROP INDEX IDX_723705D18E962C16');
        $this->addSql('ALTER TABLE transaction DROP animal_id');
        $this->addSql('ALTER TABLE transaction ALTER vendeur_id DROP NOT NULL');
        $this->addSql('ALTER INDEX idx_723705d1858c065e RENAME TO idx_723705d120068689');
        $this->addSql('ALTER INDEX idx_723705d196a7bb5f RENAME TO idx_723705d18eb576a8');
    }
}
