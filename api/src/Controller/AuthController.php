<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AuthController extends AbstractController
{
    public function register(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $em = $this->getDoctrine()->getManager();
        $password = $request->request->get('password');
        $role = 'ROLE_'.$request->request->get('role');
        $user = new User();
        $user->setName($request->request->get('name'))
            ->setEmail($request->request->get('email'))
            ->setPassword($encoder->encodePassword($user, $password))
            ->setAdresse($request->request->get('adresse'))
            ->setCp($request->request->get('cp'))
            ->setVille($request->request->get('ville'))
            ->setPays($request->request->get('pays'))
            ->setTelephone($request->request->get('telephone'))
            ->setPrismaId($request->request->get('prismaId'))
            ->setRoles(array($role));

        $em->persist($user);
        $em->flush();

        return new JsonResponse('success');
    }

}
