<?php

namespace App\DataFixtures;

use App\Entity\Animal;
use App\Entity\Certification;
use App\Entity\Commentaire;
use App\Entity\Transaction;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class AppFixtures extends Fixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create('fr_FR');
        // Create Particulier
        for ($i = 0; $i < 2; $i++) {
            $user = (new User())
                ->setName($faker->firstName." ".$faker->lastName)
                ->setPassword($faker->password)
                ->setEmail($faker->safeEmail)
                ->setRoles(['ROLE_USER'])
                ->setTelephone($faker->phoneNumber)
                ->setAdresse($faker->address)
                ->setVille($faker->city)
                ->setCp($faker->postcode)
                ->setPays($faker->country)
            ;

            $user->setPassword($this->passwordEncoder->encodePassword(
                $user,
                'test'
            ));

            $manager->persist($user);
        }
        //Create Zoo
        for ($i = 0; $i < 2; $i++) {
            $zoo = (new User())
                ->setName($faker->firstName." ".$faker->lastName)
                ->setNameZoo("Zoo")
                ->setPassword($faker->password)
                ->setEmail($faker->safeEmail)
                ->setRoles(['ROLE_ZOO'])
                ->setTelephone($faker->phoneNumber)
                ->setAdresse($faker->address)
                ->setVille($faker->city)
                ->setCp($faker->postcode)
                ->setPays($faker->country)
                ->setSiretSiren($faker->siret)
            ;

            $zoo->setPassword($this->passwordEncoder->encodePassword(
                $zoo,
                'test'
            ));

            $manager->persist($zoo);
        }
        //Create Eleveur
        for ($i = 0; $i < 2; $i++) {
            $eleveur = (new User())
                ->setName($faker->firstName." ".$faker->lastName)
                ->setPassword($faker->password)
                ->setEmail($faker->safeEmail)
                ->setRoles(['ROLE_ELEVEUR'])
                ->setTelephone($faker->phoneNumber)
                ->setAdresse($faker->address)
                ->setVille($faker->city)
                ->setCp($faker->postcode)
                ->setPays($faker->country)
                ->setSiretSiren($faker->siret)
            ;

            $eleveur->setPassword($this->passwordEncoder->encodePassword(
                $eleveur,
                'test'
            ));

            $manager->persist($eleveur);
        }
        //Create Seller
        for ($i = 0; $i < 2; $i++) {
            $seller = (new User())
                ->setName($faker->firstName." ".$faker->lastName)
                ->setPassword($faker->password)
                ->setEmail($faker->safeEmail)
                ->setRoles(['ROLE_SELLER'])
                ->setTelephone($faker->phoneNumber)
                ->setAdresse($faker->address)
                ->setVille($faker->city)
                ->setCp($faker->postcode)
                ->setPays($faker->country)
                ->setSiretSiren($faker->siret)
            ;

            $seller->setPassword($this->passwordEncoder->encodePassword(
                $seller,
                'test'
            ));

            $manager->persist($seller);
        }
        //Create Admin
        $admin = (new User())
            ->setName($faker->firstName." ".$faker->lastName)
            ->setPassword($faker->password)
            ->setEmail('user@admin')
            ->setRoles(['ROLE_ADMIN'])
        ;

        $admin->setPassword($this->passwordEncoder->encodePassword(
            $admin,
            'test'
        ));
        $manager->persist($admin);


        //Animal
        $lion = (new Animal())
            ->setEspece('Lion')
            ->setGenre('M')
            ->setRace('Lion du Congo')
            ->setDateBirth($faker->dateTimeBetween('-10 years', 'now'))
            ->setBirthplace($faker->country)
            ->setProprio($zoo)
            ->setPrix("1000000")
        ;
        $manager->persist($lion);

        $tigre = (new Animal())
            ->setEspece('Tigre')
            ->setGenre('F')
            ->setRace('Tigre Blanc')
            ->setDateBirth($faker->dateTimeBetween('-10 years', 'now'))
            ->setBirthplace($faker->country)
            ->setProprio($zoo)
            ->setPrix("1500000")
        ;
        $manager->persist($tigre);

        $ours = (new Animal())
            ->setEspece('Ours')
            ->setGenre('M')
            ->setRace('Ours brun')
            ->setDateBirth($faker->dateTimeBetween('-10 years', 'now'))
            ->setBirthplace($faker->country)
            ->setPrix("100000")
            ->setProprio($eleveur)
        ;
        $manager->persist($ours);

        $oursp = (new Animal())
            ->setEspece('Ours')
            ->setGenre('F')
            ->setRace('Ours Polaire')
            ->setDateBirth($faker->dateTimeBetween('-10 years', 'now'))
            ->setBirthplace($faker->country)
            ->setPrix("150000")
            ->setProprio($user)
        ;
        $this->addReference('ref-oursp', $oursp);
        $manager->persist($oursp);

        $coyote = (new Animal())
            ->setEspece('Coyote')
            ->setGenre('F')
            ->setRace('Coyote des montagnes')
            ->setDateBirth($faker->dateTimeBetween('-10 years', 'now'))
            ->setBirthplace($faker->country)
            ->setPrix("50000")
            ->setProprio($eleveur)
        ;
        $manager->persist($coyote);

        $phacochere = (new Animal())
            ->setEspece("Phacochère")
            ->setGenre('M')
            ->setRace('Phacochère africanus')
            ->setDateBirth($faker->dateTimeBetween('-10 years', 'now'))
            ->setBirthplace($faker->country)
            ->setProprio($seller)
            ->setPrix("25000")
        ;
        $manager->persist($phacochere);

        $suricate = (new Animal())
            ->setEspece('Suricate')
            ->setGenre('M')
            ->setRace('Suricate suricata marjoriae')
            ->setDateBirth($faker->dateTimeBetween('-10 years', 'now'))
            ->setBirthplace($faker->country)
            ->setProprio($seller)
            ->setPrix("20000")
        ;
        $manager->persist($suricate);

        $singe = (new Animal())
            ->setEspece('Babouins')
            ->setGenre('F')
            ->setRace('Mandrill')
            ->setDateBirth($faker->dateTimeBetween('-10 years', 'now'))
            ->setBirthplace($faker->country)
            ->setProprio($user)
            ->setPrix("75000")
        ;
        $this->addReference('ref-singe', $singe);
        $manager->persist($singe);

        //Create Certification
        $certif = (new Certification())
            ->setIdUser($zoo)
            ->setNom('Certification Lion')
            ->setDateObtention($faker->dateTimeBetween('-10 years', 'now'))
            ->setValidation(true)
        ;
        $manager->persist($certif);
        $certif = (new Certification())
            ->setIdUser($zoo)
            ->setNom('Certification Tigre')
            ->setDateObtention($faker->dateTimeBetween('-10 years', 'now'))
            ->setValidation(true)
        ;
        $manager->persist($certif);
        $certif = (new Certification())
            ->setIdUser($eleveur)
            ->setNom('Certification Ours')
            ->setDateObtention($faker->dateTimeBetween('-10 years', 'now'))
            ->setValidation(true)
        ;
        $manager->persist($certif);
        $certif = (new Certification())
            ->setIdUser($zoo)
            ->setNom('Certification Coyote')
            ->setDateObtention($faker->dateTimeBetween('-10 years', 'now'))
            ->setValidation(true)
        ;
        $manager->persist($certif);
        $certif = (new Certification())
            ->setIdUser($user)
            ->setNom('Certification Ours')
            ->setDateObtention($faker->dateTimeBetween('-10 years', 'now'))
            ->setValidation(true)
        ;
        $manager->persist($certif);
        $certif = (new Certification())
            ->setIdUser($seller)
            ->setNom('Certification Babouin')
            ->setDateObtention($faker->dateTimeBetween('-10 years', 'now'))
            ->setValidation(true)
        ;
        $manager->persist($certif);
        $certif = (new Certification())
            ->setIdUser($user)
            ->setNom('Certification Babouin')
            ->setDateObtention($faker->dateTimeBetween('-10 years', 'now'))
            ->setValidation(true)
        ;
        $manager->persist($certif);
        //Create Comment
        $comment = (new Commentaire())
            ->setUserSubject($eleveur)
            ->setIdAuthor($user)
            ->setNote('4')
            ->setComment($faker->text())
        ;
        $manager->persist($comment);
        $comment = (new Commentaire())
            ->setUserSubject($seller)
            ->setIdAuthor($user)
            ->setNote('5')
            ->setComment($faker->text())
        ;
        $manager->persist($comment);

        //Create transaction
        $transaction = (new Transaction())
            ->setAcheteur($user)
            ->setVendeur($eleveur)
            ->setPrix("150000")
            ->setType('achat')
            ->setStatut(1)
            ->addAnimal($this->getReference('ref-oursp'))
        ;
        $manager->persist($transaction);
        $transaction = (new Transaction())
            ->setAcheteur($user)
            ->setVendeur($seller)
            ->setPrix("75000")
            ->setType('achat')
            ->setStatut(1)
            ->addAnimal($this->getReference('ref-singe'))
        ;
        $manager->persist($transaction);

        $manager->flush();
    }

}
