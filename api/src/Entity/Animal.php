<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\AnimalRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"animal_default"}},
 *     itemOperations={
 *         "get"={
 *              "normalization_context"={"groups"={"animal_get"}},
 *         },
 *         "delete",
 *         "put",
 *         "patch"
 *     }
 * )
 * @ORM\Entity(repositoryClass=AnimalRepository::class)
 * @ApiFilter(SearchFilter::class, properties={
 *     "prismaId": "exact",
 * })
 * @ORM\HasLifecycleCallbacks()
 */
class Animal
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"animal_default", "animal_get", "user_default", "user_get"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"animal_default", "animal_get", "user_default", "user_get", "transaction_default", "transaction_get"})
     */
    private $espece;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"animal_default", "animal_get", "user_default", "user_get", "transaction_default", "transaction_get"})
     */
    private $race;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"animal_default", "animal_get", "user_default", "user_get", "transaction_default", "transaction_get"})
     */
    private $genre;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Groups({"animal_default", "animal_get", "user_default", "user_get"})
     */
    private $dateBirth;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"animal_default", "animal_get", "user_default", "user_get"})
     */
    private $birthplace;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="idAnimals")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"animal_default", "animal_get"})
     */
    private $proprio;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"animal_get"})
     */
    private $createAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"animal_get"})
     */
    private $updateAt;

    /**
     * @ORM\OneToMany(targetEntity=Transaction::class, mappedBy="animals")
     * @Groups({"animal_default", "animal_get"})
     */
    private $transactions;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Groups({"animal_default", "animal_get"})
     */
    private $prix;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"animal_default", "animal_get", "user_default", "user_get", "transaction_default"})
     */
    private $prismaId;


    public function __construct()
    {
        $this->transactions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEspece(): ?string
    {
        return $this->espece;
    }

    public function setEspece(?string $espece): self
    {
        $this->espece = $espece;

        return $this;
    }

    public function getRace(): ?string
    {
        return $this->race;
    }

    public function setRace(?string $race): self
    {
        $this->race = $race;

        return $this;
    }

    public function getGenre(): ?string
    {
        return $this->genre;
    }

    public function setGenre(?string $genre): self
    {
        $this->genre = $genre;

        return $this;
    }

    public function getDateBirth(): ?\DateTimeInterface
    {
        return $this->dateBirth;
    }

    public function setDateBirth(?\DateTimeInterface $dateBirth): self
    {
        $this->dateBirth = $dateBirth;

        return $this;
    }

    public function getBirthplace(): ?string
    {
        return $this->birthplace;
    }

    public function setBirthplace(?string $birthplace): self
    {
        $this->birthplace = $birthplace;

        return $this;
    }

    public function getProprio(): ?User
    {
        return $this->proprio;
    }

    public function setProprio(?User $proprio): self
    {
        $this->proprio = $proprio;

        return $this;
    }

    public function getCreateAt(): ?\DateTimeInterface
    {
        return $this->createAt;
    }

    public function setCreateAt(\DateTimeInterface $createAt): self
    {
        $this->createAt = $createAt;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->updateAt;
    }

    public function setUpdateAt(?\DateTimeInterface $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    /**
     * @return Collection|Transaction[]
     */
    public function getTransactions(): Collection
    {
        return $this->transactions;
    }

    public function addTransaction(Transaction $transaction): self
    {
        if (!$this->transactions->contains($transaction)) {
            $this->transactions[] = $transaction;
            $transaction->setAnimal($this);
        }

        return $this;
    }

    public function removeTransaction(Transaction $transaction): self
    {
        if ($this->transactions->removeElement($transaction)) {
            // set the owning side to null (unless already changed)
            if ($transaction->getAnimals() === $this) {
                $transaction->setAnimals(null);
            }

        }

        return $this;
    }

    public function getPrix(): ?string
    {
        return $this->prix;
    }

    public function setPrix(?string $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps(): void
    {
        $dateTimeNow = new \DateTime('now');

        $this->setUpdateAt($dateTimeNow);

        if ($this->getCreateAt() === null) {
            $this->setCreateAt($dateTimeNow);
        }
    }

    public function getPrismaId(): ?string
    {
        return $this->prismaId;
    }

    public function setPrismaId(?string $prismaId): self
    {
        $this->prismaId = $prismaId;

        return $this;
    }

}
