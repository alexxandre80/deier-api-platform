<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\CertificationRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"certif_default"}},
 *     itemOperations={
 *         "get"={
 *              "normalization_context"={"groups"={"certif_get"}},
 *         },
 *         "delete",
 *         "put",
 *         "patch"
 *     }
 * )
 * @ORM\Entity(repositoryClass=CertificationRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class Certification
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"certif_default", "certif_get", "user_default", "user_get"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"certif_default", "certif_get", "user_default", "user_get"})
     */
    private $nom;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"certif_get"})
     */
    private $createAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"certif_get"})
     */
    private $updateAt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"certif_default", "certif_get"})
     */
    private $path;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"certif_default", "certif_get", "user_default", "user_get"})
     */
    private $validation = false;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"certif_default", "certif_get"})
     */
    private $dateObtention;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="UserCertif")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"certif_default", "certif_get"})
     */
    private $idUser;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"certif_default", "certif_get", "user_default", "user_get"})
     */
    private $prismaId;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getCreateAt(): ?\DateTimeInterface
    {
        return $this->createAt;
    }

    public function setCreateAt(\DateTimeInterface $createAt): self
    {
        $this->createAt = $createAt;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->updateAt;
    }

    public function setUpdateAt(?\DateTimeInterface $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function setPath(?string $path): self
    {
        $this->path = $path;

        return $this;
    }

    public function getValidation(): ?bool
    {
        return $this->validation;
    }

    public function setValidation(bool $validation): self
    {
        $this->validation = $validation;

        return $this;
    }

    public function getDateObtention(): ?\DateTimeInterface
    {
        return $this->dateObtention;
    }

    public function setDateObtention(?\DateTimeInterface $dateObtention): self
    {
        $this->dateObtention = $dateObtention;

        return $this;
    }

    public function getIdUser(): ?User
    {
        return $this->idUser;
    }

    public function setIdUser(?User $idUser): self
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps(): void
    {
        $dateTimeNow = new \DateTime('now');

        $this->setUpdateAt($dateTimeNow);

        if ($this->getCreateAt() === null) {
            $this->setCreateAt($dateTimeNow);
        }
    }

    public function getPrismaId(): ?string
    {
        return $this->prismaId;
    }

    public function setPrismaId(?string $prismaId): self
    {
        $this->prismaId = $prismaId;

        return $this;
    }

}
