<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\CommentaireRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"comment_default"}},
 *     itemOperations={
 *         "get"={
 *              "normalization_context"={"groups"={"comment_get"}},
 *         },
 *         "delete",
 *         "put",
 *         "patch"
 *     }
 * )
 * @ORM\Entity(repositoryClass=CommentaireRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class Commentaire
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"comment_default", "comment_get", "user_default", "user_get"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="idCommentaires")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"comment_default", "comment_get"})
     */
    private $idAuthor;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"comment_default", "comment_get", "user_default", "user_get"})
     */
    private $note;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"comment_default", "comment_get", "user_default", "user_get"})
     */
    private $comment;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"comment_get", "user_get"})
     */
    private $createAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"comment_get", "user_default", "user_get"})
     */
    private $updateAt;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="UserSubjectComment")
     * @Groups({"comment_default", "comment_get"})
     */
    private $userSubject;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"comment_default", "comment_get", "user_default", "user_get"})
     */
    private $prismaId;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdAuthor(): ?User
    {
        return $this->idAuthor;
    }

    public function setIdAuthor(?User $idAuthor): self
    {
        $this->idAuthor = $idAuthor;

        return $this;
    }

    public function getNote(): ?string
    {
        return $this->note;
    }

    public function setNote(?string $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getCreateAt(): ?\DateTimeInterface
    {
        return $this->createAt;
    }

    public function setCreateAt(\DateTimeInterface $createAt): self
    {
        $this->createAt = $createAt;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->updateAt;
    }

    public function setUpdateAt(?\DateTimeInterface $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    public function getUserSubject(): ?User
    {
        return $this->userSubject;
    }

    public function setUserSubject(?User $userSubject): self
    {
        $this->userSubject = $userSubject;

        return $this;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps(): void
    {
        $dateTimeNow = new \DateTime('now');

        $this->setUpdateAt($dateTimeNow);

        if ($this->getCreateAt() === null) {
            $this->setCreateAt($dateTimeNow);
        }
    }

    public function getPrismaId(): ?string
    {
        return $this->prismaId;
    }

    public function setPrismaId(?string $prismaId): self
    {
        $this->prismaId = $prismaId;

        return $this;
    }

}
