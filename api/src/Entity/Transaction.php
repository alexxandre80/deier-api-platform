<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use App\Repository\TransactionRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"transaction_default"}},
 *     itemOperations={
 *         "get"={
 *              "normalization_context"={"groups"={"transaction_get"}},
 *         },
 *         "delete",
 *         "put",
 *         "patch"
 *     }
 * )
 * @ORM\Entity(repositoryClass=TransactionRepository::class)
 * @ApiFilter(SearchFilter::class, properties={
 *     "prismaId": "exact",
 *     "acheteur": "exact",
 *     "vendeur": "exact",
 * })
 * @ORM\HasLifecycleCallbacks()
 */
class Transaction
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"transaction_default", "transaction_get", "user_default", "user_get", "animal_get"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="idUserAcheteur")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"transaction_default", "transaction_get"})
     */
    private $acheteur;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="idUserVendeur")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"transaction_default", "transaction_get"})
     */
    private $vendeur;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"transaction_default", "transaction_get", "user_default", "user_get", "animal_get"})
     */
    private $type;

    /**
     * @ORM\Column(type="string")
     * @Groups({"transaction_default", "transaction_get", "user_default", "user_get", "animal_get"})
     */
    private $prix;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"transaction_get", "user_default", "user_get", "animal_get"})
     */
    private $createAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"transaction_get"})
     */
    private $updateAt;

    /**
     * @ORM\ManyToOne(targetEntity=Animal::class, inversedBy="transactions")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"transaction_default", "transaction_get"})
     */
    private $animals;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"transaction_default", "transaction_get", "user_default", "user_get", "animal_get"})
     */
    private $statut = 0;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"transaction_default", "transaction_get", "user_default", "user_get", "animal_get"})
     */
    private $prismaId;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAcheteur(): ?User
    {
        return $this->acheteur;
    }

    public function setAcheteur(User $acheteur): self
    {
        $this->acheteur = $acheteur;

        return $this;
    }

    public function getVendeur(): ?User
    {
        return $this->vendeur;
    }

    public function setVendeur(User $vendeur): self
    {
        $this->vendeur = $vendeur;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getPrix(): ?string
    {
        return $this->prix;
    }

    public function setPrix(string $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    public function getCreateAt(): ?\DateTimeInterface
    {
        return $this->createAt;
    }

    public function setCreateAt(\DateTimeInterface $createAt): self
    {
        $this->createAt = $createAt;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->updateAt;
    }

    public function setUpdateAt(?\DateTimeInterface $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    public function getAnimals(): ?Animal
    {
        return $this->animals;
    }

    public function setAnimals(Animal $animals): self
    {
        $this->animals = $animals;
        return $this;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps(): void
    {
        $dateTimeNow = new \DateTime('now');

        $this->setUpdateAt($dateTimeNow);

        if ($this->getCreateAt() === null) {
            $this->setCreateAt($dateTimeNow);
        }
    }

    public function getStatut(): ?int
    {
        return $this->statut;
    }

    public function setStatut(?int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getPrismaId(): ?string
    {
        return $this->prismaId;
    }

    public function setPrismaId(?string $prismaId): self
    {
        $this->prismaId = $prismaId;

        return $this;
    }

}
