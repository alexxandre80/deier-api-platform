<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"user_default"}},
 *     itemOperations={
 *         "get"={
 *              "normalization_context"={"groups"={"user_get"}},
 *         },
 *         "delete",
 *         "put",
 *         "patch"
 *     }
 * )
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(name="`user`")
 * @ApiFilter(SearchFilter::class, properties={
 *     "prismaId": "exact",
 * })
 * @ORM\HasLifecycleCallbacks()
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"user_default", "user_get", "animal_default", "animal_get", "comment_get", "certif_get", "transaction_default", "transaction_get"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Groups({"user_default", "user_get", "transaction_default", "animal_default",})
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     * @Groups({"user_default"})
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     * @Groups({"user_default"})
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"user_default", "user_get", "animal_default", "animal_get", "comment_get", "certif_get", "transaction_default", "transaction_get"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     * @Groups({"user_default", "user_get"})
     */
    private $siretSiren;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"user_default", "user_get", "animal_default", "transaction_default"})
     */
    private $adresse;

    /**
     * @ORM\Column(type="string", length=125, nullable=true)
     * @Groups({"user_default", "user_get", "animal_default", "transaction_default"})
     */
    private $cp;

    /**
     * @ORM\Column(type="string", length=125, nullable=true)
     * @Groups({"user_default", "user_get", "animal_default", "transaction_default"})
     */
    private $ville;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"user_default", "user_get", "animal_default", "transaction_default"})
     */
    private $pays;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"user_default", "user_get"})
     */
    private $telephone;

    /**
     * @ORM\OneToMany(targetEntity=Animal::class, mappedBy="proprio", orphanRemoval=true)
     * @Groups({"user_default", "user_get"})
     */
    private $idAnimals;

    /**
     * @ORM\OneToMany(targetEntity=Commentaire::class, mappedBy="idAuthor", orphanRemoval=true)
     * @Groups({"user_default", "user_get"})
     */
    private $idCommentaires;

    /**
     * @ORM\OneToMany(targetEntity=Commentaire::class, mappedBy="userSubject")
     * @Groups({"user_default", "user_get"})
     */
    private $UserSubjectComment;

    /**
     * @ORM\OneToMany(targetEntity=Transaction::class, mappedBy="acheteur")
     * @Groups({"user_default", "user_get"})
     */
    private $idUserAcheteur;

    /**
     * @ORM\OneToMany (targetEntity=Transaction::class, mappedBy="vendeur")
     * @Groups({"user_default", "user_get"})
     */
    private $idUserVendeur;

    /**
     * @ORM\OneToMany(targetEntity=Certification::class, mappedBy="idUser", orphanRemoval=true)
     * @Groups({"user_default","user_get"})
     */
    private $UserCertif;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"user_get"})
     */
    private $createAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"user_default", "user_get"})
     */
    private $updateAt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"user_default", "user_get", "animal_get", "transaction_get"})
     */
    private $nameZoo;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"user_default", "user_get", "animal_default", "animal_get", "comment_get", "certif_get", "transaction_default", "transaction_get"})
     */
    private $prismaId;

    public function __construct()
    {
        $this->idAnimals = new ArrayCollection();
        $this->idCommentaires = new ArrayCollection();
        $this->UserCertif = new ArrayCollection();
        $this->UserSubjectComment = new ArrayCollection();
        $this->idUserAcheteur = new ArrayCollection();
        $this->idUserVendeur = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSiretSiren(): ?string
    {
        return $this->siretSiren;
    }

    public function setSiretSiren(?string $siretSiren): self
    {
        $this->siretSiren = $siretSiren;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(?string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getCp(): ?string
    {
        return $this->cp;
    }

    public function setCp(string $cp): self
    {
        $this->cp = $cp;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(?string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getPays(): ?string
    {
        return $this->pays;
    }

    public function setPays(?string $pays): self
    {
        $this->pays = $pays;

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(?string $telephone): self
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * @return Collection|Animal[]
     */
    public function getIdAnimals(): Collection
    {
        return $this->idAnimals;
    }

    public function addIdAnimal(Animal $idAnimal): self
    {
        if (!$this->idAnimals->contains($idAnimal)) {
            $this->idAnimals[] = $idAnimal;
            $idAnimal->setIdUser($this);
        }

        return $this;
    }

    public function removeIdAnimal(Animal $idAnimal): self
    {
        if ($this->idAnimals->contains($idAnimal)) {
            $this->idAnimals->removeElement($idAnimal);
            // set the owning side to null (unless already changed)
            if ($idAnimal->getIdUser() === $this) {
                $idAnimal->setIdUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Commentaire[]
     */
    public function getIdCommentaires(): Collection
    {
        return $this->idCommentaires;
    }

    public function addIdCommentaire(Commentaire $idCommentaire): self
    {
        if (!$this->idCommentaires->contains($idCommentaire)) {
            $this->idCommentaires[] = $idCommentaire;
            $idCommentaire->setIdAuthor($this);
        }

        return $this;
    }

    public function removeIdCommentaire(Commentaire $idCommentaire): self
    {
        if ($this->idCommentaires->contains($idCommentaire)) {
            $this->idCommentaires->removeElement($idCommentaire);
            // set the owning side to null (unless already changed)
            if ($idCommentaire->getIdAuthor() === $this) {
                $idCommentaire->setIdAuthor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Commentaire[]
     */
    public function getUserSubjectComment(): Collection
    {
        return $this->UserSubjectComment;
    }

    public function addUserSubjectComment(Commentaire $userSubjectComment): self
    {
        if (!$this->UserSubjectComment->contains($userSubjectComment)) {
            $this->UserSubjectComment[] = $userSubjectComment;
            $userSubjectComment->setUserSubject($this);
        }

        return $this;
    }

    public function removeUserSubjectComment(Commentaire $userSubjectComment): self
    {
        if ($this->UserSubjectComment->removeElement($userSubjectComment)) {
            // set the owning side to null (unless already changed)
            if ($userSubjectComment->getUserSubject() === $this) {
                $userSubjectComment->setUserSubject(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Transaction[]
     */
    public function getIdUserAcheteur(): Collection
    {
        return $this->idUserAcheteur;
    }

    public function addIdUserAcheteur(Transaction $idUserAcheteur): self
    {
        if (!$this->idUserAcheteur->contains($idUserAcheteur)) {
            $this->idUserAcheteur[] = $idUserAcheteur;
            $idUserAcheteur->setIdAcheteur($this);
        }

        return $this;
    }

    public function removeIdUserAcheteur(Transaction $idUserAcheteur): self
    {
        if ($this->idUserAcheteur->removeElement($idUserAcheteur)) {
            // set the owning side to null (unless already changed)
            if ($idUserAcheteur->getIdAcheteur() === $this) {
                $idUserAcheteur->setIdAcheteur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Transaction[]
     */
    public function getIdUserVendeur(): Collection
    {
        return $this->idUserVendeur;
    }

    public function addIdUserVendeur(Transaction $idUserVendeur): self
    {
        if (!$this->idUserVendeur->contains($idUserVendeur)) {
            $this->idUserVendeur[] = $idUserVendeur;
            $idUserVendeur->setIdAcheteur($this);
        }

        return $this;
    }

    public function removeIdUserVendeur(Transaction $idUserVendeur): self
    {
        if ($this->idUserVendeur->removeElement($idUserVendeur)) {
            // set the owning side to null (unless already changed)
            if ($idUserVendeur->getIdAcheteur() === $this) {
                $idUserVendeur->setIdAcheteur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Certification[]
     */
    public function getUserCertif(): Collection
    {
        return $this->UserCertif;
    }

    public function addUserCertif(Certification $userCertif): self
    {
        if (!$this->UserCertif->contains($userCertif)) {
            $this->UserCertif[] = $userCertif;
            $userCertif->setIdUser($this);
        }

        return $this;
    }

    public function removeUserCertif(Certification $userCertif): self
    {
        if ($this->UserCertif->contains($userCertif)) {
            $this->UserCertif->removeElement($userCertif);
            // set the owning side to null (unless already changed)
            if ($userCertif->getIdUser() === $this) {
                $userCertif->setIdUser(null);
            }
        }

        return $this;
    }

    public function getCreateAt(): ?\DateTimeInterface
    {
        return $this->createAt;
    }

    public function setCreateAt(\DateTimeInterface $createAt): self
    {
        $this->createAt = $createAt;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->updateAt;
    }

    public function setUpdateAt(?\DateTimeInterface $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps(): void
    {
        $dateTimeNow = new \DateTime('now');

        $this->setUpdateAt($dateTimeNow);

        if ($this->getCreateAt() === null) {
            $this->setCreateAt($dateTimeNow);
        }
    }

    public function getNameZoo(): ?string
    {
        return $this->nameZoo;
    }

    public function setNameZoo(?string $nameZoo): self
    {
        $this->nameZoo = $nameZoo;

        return $this;
    }

    public function getPrismaId(): ?string
    {
        return $this->prismaId;
    }

    public function setPrismaId(?string $prismaId): self
    {
        $this->prismaId = $prismaId;

        return $this;
    }

}
