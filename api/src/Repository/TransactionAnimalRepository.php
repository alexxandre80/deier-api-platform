<?php

namespace App\Repository;

use App\Entity\TransactionAnimal;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TransactionAnimal|null find($id, $lockMode = null, $lockVersion = null)
 * @method TransactionAnimal|null findOneBy(array $criteria, array $orderBy = null)
 * @method TransactionAnimal[]    findAll()
 * @method TransactionAnimal[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TransactionAnimalRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TransactionAnimal::class);
    }

    // /**
    //  * @return TransactionAnimal[] Returns an array of TransactionAnimal objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TransactionAnimal
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
